import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Vuetify from 'vuetify'
import {HTTP} from "./http-common";

import vueMoment from 'vue-moment';
import moment from 'moment';
require('moment/locale/fr');

import VueNumerals from 'vue-numerals';


Vue.config.productionTip = false;

import '@public/material.css'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(vueMoment, {moment});
Vue.use(VueNumerals, {locale: "fr"});

Vue.use(Vuetify,
{
  theme:
  {
    primary: process.env.VUE_APP_THEME_PRIMARY,
    secondary: process.env.VUE_APP_THEME_SECONDARY,
    accent: process.env.VUE_APP_THEME_ACCENT,
    error: process.env.VUE_APP_THEME_ERROR,
    info: process.env.VUE_APP_THEME_INFO,
    success: process.env.VUE_APP_THEME_SUCCESS,
    warning: process.env.VUE_APP_THEME_WARNING
  }
});

Vue.prototype.$http = HTTP;

new Vue(
{
  router,
  store,
  render: h => h(App)
}).$mount('#app');
