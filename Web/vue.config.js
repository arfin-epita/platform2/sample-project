const path = require('path');

module.exports =
{
  pages:
  {
    index:
    {
      entry: 'src/main.js',
      // the source template
      template: 'public/index.html',
      // output as dist/index.html
      filename: 'index.html',

      title: "Sample Projet List"
    }
  },
  configureWebpack: () =>
  {
    return {
      resolve:
      {
        alias:
        {
          //'vue$': 'vue/dist/vue.esm.js', // 'vue/dist/vue.common.js' for webpack 1
          '@src': path.resolve(__dirname, 'src'),
          "@public": path.resolve(__dirname, "public"),
        }
      }
    }
  },
  devServer:
  {
    disableHostCheck: true
  }
};