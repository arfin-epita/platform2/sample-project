require('module-alias/register');

// Middleware
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const mongoSanitize = require('express-mongo-sanitize');
const config = require('config');

// Handlers
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');
const mongooseHandler = require('@mongooseHandler');

mongooseHandler.connect(config.get("db.localMongoDB"));

// Router
const router = require('./router');


let app = express();


app.use(morgan('dev', {stream: logHandler.stream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser("key"));
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
app.use(mongoSanitize());
app.use(cors());


app.use('/api', router);


// catch 404 and forward to error handler
app.use((req, res, next) =>
{
  return next(errorHandler.create(404, "Not found"));
});


// error handler
app.use(function(err, req, res, next)
{
  return errorHandler.send(res, err);
});



module.exports = app;
