const express = require('express');
const router = express.Router();

// Handlers
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');
const reqParamsHandler = require('@reqParamsHandler');

// Lib
const lib = require('../library/index');



/**
 * @api {get} /ms-activities/ping Ping status of the service
 * @apiName Get Ping
 * @apiGroup MS-Activities
 */
router.get('/ping', (req, res, next) =>
{
  return res.json({message: "Hello world !"});
});


router.get('/', (req, res, next) =>
{
  lib
  .getActivities()
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});


router.post('/', (req, res, next) =>
{

  lib
  .insertActivity(req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


router.delete('/:id', (req, res, next) =>
{
  lib
  .deleteActivity(req.params.id)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


module.exports = router;
