
// Handlers
const errorHandler = require('@errorHandler');
const logger = require('@logHandler');

// DataAccess
const dataAccess = require('../dataAccess/index');



function getActivities()
{
  return new Promise((resolve, reject) =>
  {
    dataAccess
    .findActivities()
    .then(data =>
    {
      return resolve({items: data});
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}


function insertActivity(obj)
{
  return new Promise(async (resolve, reject) =>
  {

    if (typeof obj === "object" && obj["content"])
    {
      dataAccess
      .insert({content: obj.content, date: new Date()})
      .then(data =>
      {
        return resolve(data);
      })
      .catch(err =>
      {
        return reject(errorHandler.cast(err));
      })
    }
    else
      return reject(errorHandler.create(400));
  })
}

function deleteActivity(id)
{
  return new Promise((resolve, reject) =>
  {
    dataAccess
    .deleteByID(id)
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })


  })
}

module.exports =
{
  getActivities,
  insertActivity,
  deleteActivity
};